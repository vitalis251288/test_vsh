!(function ($) {

    var global_scripts = {
        header: function () {
            var $main = $(".main"),
                $header = $(".header"),
                $header_bottom = $header.find(".header_bottom");
            $main.css({
                'padding-top': $header.height() + 'px'
            });
            if($(window).scrollTop() >= ($header.height() + $header_bottom.height())){
                $header.addClass('fixed');
            }else{
                $header.removeClass('fixed');
            };
        },
        slider: function () {
            var $slider = $(".slider");
            if($slider.length){
                $slider.each(function (i, el) {
                    $(this).owlCarousel({
                        items: 1,
                        dots: false,
                        nav: true,
                        loop: true,
                        navText: ['<i class="fa-arrow-left"></i>', '<i class="fa-arrow-right"></i>']
                    });
                });
            }
        },
        footer: function () {

        },
        spec_box: function () {
            var $spec_box = $(".spec_box");
            if($spec_box.length){
                /*var $div = $spec_box.find("> div");
                $div.removeAttr("style");
                var max = 0;
                $div.each(function (index, elem) {
                    if($(this).height() >= 0){
                        max = $(this).height();
                    };
                    //return max;
                });
                $spec_box.find("> div").height(max);*/
                $spec_box.each(function (i, el) {
                    var $h_img = $(this).find(".spec_box_image").height(),
                        $h_info = $(this).find(".spec_box_info").height();
                    setTimeout(function () {
                        $(this).find(".spec_box_info .spec_box_info_inner").removeAttr("style");
                        if($h_img >= $h_info){
                            $(this).find(".spec_box_info .spec_box_info_inner").height(($h_img - 70 + 'px'));
                        }else{
                            $(this).find(".spec_box_info .spec_box_info_inner").removeAttr("style");
                        };
                    }, 0);
                });
            };
        },
        map: function () {
            var $map = $("#map");
            function initMap() {
                var myLatLng = {lat: -25.363, lng: 131.044};

                // Create a map object and specify the DOM element for display.
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: myLatLng,
                    zoom: 4
                });

                // Create a marker and set its position.
                var marker = new google.maps.Marker({
                    map: map,
                    position: myLatLng,
                    title: 'Hello World!'
                });
            }initMap();
        },
        slide_menu: function() {
            var $hcm = $('.js_mobile_btn'),
                $master = $('#wrapper'),
                $slideMenu = $('#slide_menu'),
                $nano = $('.nano');
            if ($nano.length) {
                $hcm.on('click', function (e) {
                    $hcm.toggleClass("active");
                    $('body').toggleClass("ov_h");
                    $master.toggleClass("slided");
                    $slideMenu.toggleClass("slided");
                    return false;
                });
                $('.slideMenuClose').on('click', function () {
                    console.log("close");
                    $hcm.removeClass('active');
                    $('body').removeClass("ov_h");
                    $master.removeClass('slided');
                    $slideMenu.removeClass('slided');
                });
                $nano.nanoScroller({
                    preventPageScrolling: true,
                    alwaysVisible: false,
                    iOSNativeScrolling: true
                });
            }
        },
        init: function () {
            this.header();
            this.slider();
            //this.spec_box();
            this.map();
            this.footer();
            this.slide_menu();
        }
    };

    $(document).on("ready", function(){

        //$(window).trigger("scroll", "resize");

        global_scripts.init();

        $(window).on("resize", function () {
            global_scripts.header();
            //global_scripts.spec_box();
        });

        $(window).on("scroll", function () {
            global_scripts.header();
            //global_scripts.spec_box();
        });

    });

})(jQuery);